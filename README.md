# Alfred Inflow Example Project
Alfred Inflow and Alfresco setup configured with an out of the box Inflow metadata action

## Prerequisites
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

## Setting up the example
This project provides a `docker-compose` file, which contains: 

* an Alfresco with the Inflow backend module installed
* the Alfred Inflow client application

To start the entire example just run the `docker-compose up -d` command.

Once Alfred Inflow and Alfresco are started, they are available on following local addresses:

* http://localhost:8080/inflow
* http://localhost:8081/alfresco

Both applications can be accessed with the username 'admin' and password 'admin'

## Links
* [Alfred Inflow website](https://xenit.eu/alfred-inflow-content-migration-alfresco/)
* [Alfred Inflow documentation](https://docs.xenit.eu/alfred-inflow/)