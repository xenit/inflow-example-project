-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: inflow
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.38-MariaDB-1~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `JobModel_cron`
--

DROP TABLE IF EXISTS `JobModel_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobModel_cron` (
  `JobModel_id` int(11) NOT NULL,
  `cron` varchar(255) DEFAULT NULL,
  KEY `FKk9wsgo70g84p6fvbtnlpvfal` (`JobModel_id`),
  CONSTRAINT `FKk9wsgo70g84p6fvbtnlpvfal` FOREIGN KEY (`JobModel_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobModel_cron`
--

LOCK TABLES `JobModel_cron` WRITE;
/*!40000 ALTER TABLE `JobModel_cron` DISABLE KEYS */;
/*!40000 ALTER TABLE `JobModel_cron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JobModel_inputFolders`
--

DROP TABLE IF EXISTS `JobModel_inputFolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobModel_inputFolders` (
  `JobModel_id` int(11) NOT NULL,
  `inputFolders` varchar(255) DEFAULT NULL,
  KEY `FK3g6qh6sqvwts7igumbcpb63vs` (`JobModel_id`),
  CONSTRAINT `FK3g6qh6sqvwts7igumbcpb63vs` FOREIGN KEY (`JobModel_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobModel_inputFolders`
--

LOCK TABLES `JobModel_inputFolders` WRITE;
/*!40000 ALTER TABLE `JobModel_inputFolders` DISABLE KEYS */;
INSERT INTO `JobModel_inputFolders` VALUES (1,'/data/FilesystemMetadataAction/input');
/*!40000 ALTER TABLE `JobModel_inputFolders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JobModel_paramMetadata`
--

DROP TABLE IF EXISTS `JobModel_paramMetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobModel_paramMetadata` (
  `JobModel_id` int(11) NOT NULL,
  `paramMetadata` varchar(255) DEFAULT NULL,
  KEY `FKfpuv7f85b8pywe1o6cfhh2eee` (`JobModel_id`),
  CONSTRAINT `FKfpuv7f85b8pywe1o6cfhh2eee` FOREIGN KEY (`JobModel_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobModel_paramMetadata`
--

LOCK TABLES `JobModel_paramMetadata` WRITE;
/*!40000 ALTER TABLE `JobModel_paramMetadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `JobModel_paramMetadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JobModel_paramTransform`
--

DROP TABLE IF EXISTS `JobModel_paramTransform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JobModel_paramTransform` (
  `JobModel_id` int(11) NOT NULL,
  `paramTransform` varchar(255) DEFAULT NULL,
  KEY `FKr3m5jmdych36drrtuajyankw8` (`JobModel_id`),
  CONSTRAINT `FKr3m5jmdych36drrtuajyankw8` FOREIGN KEY (`JobModel_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JobModel_paramTransform`
--

LOCK TABLES `JobModel_paramTransform` WRITE;
/*!40000 ALTER TABLE `JobModel_paramTransform` DISABLE KEYS */;
/*!40000 ALTER TABLE `JobModel_paramTransform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cycle`
--

DROP TABLE IF EXISTS `cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cycle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endDateTime` datetime DEFAULT NULL,
  `startDateTime` datetime DEFAULT NULL,
  `totalDocumentsInInput` bigint(20) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKj8d1mkl3w5t3dq7le9jnklf6u` (`job_id`),
  CONSTRAINT `FKj8d1mkl3w5t3dq7le9jnklf6u` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cycle`
--

LOCK TABLES `cycle` WRITE;
/*!40000 ALTER TABLE `cycle` DISABLE KEYS */;
INSERT INTO `cycle` VALUES (1,'2019-04-17 10:56:41','2019-04-17 10:56:39',0,1),(2,'2019-04-17 10:59:17','2019-04-17 10:59:15',4,1);
/*!40000 ALTER TABLE `cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination` (
  `id` int(11) NOT NULL,
  `alfrescoUrl` longtext NOT NULL,
  `createNodeThreadsNbr` int(11) NOT NULL,
  `maxBufferedReports` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `nodeTransactionSize` int(11) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `storeContentThreadsNbr` int(11) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination`
--

LOCK TABLES `destination` WRITE;
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
INSERT INTO `destination` VALUES (1,'http://alfresco:8080/alfresco/s',4,2000,'docker-alfresco',250,'admin',2,NULL,'admin');
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `document`
--

DROP TABLE IF EXISTS `document`;
/*!50001 DROP VIEW IF EXISTS `document`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `document` AS SELECT 
 1 AS `cycle_id`,
 1 AS `name`,
 1 AS `document_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (2);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allowResidualProperties` bit(1) DEFAULT NULL,
  `batchSize` int(11) DEFAULT NULL,
  `cmisPassword` varchar(255) DEFAULT NULL,
  `cmisQuery` longtext,
  `cmisQueryMaxLength` int(11) NOT NULL,
  `cmisRecursive` bit(1) DEFAULT NULL,
  `cmisURL` varchar(255) DEFAULT NULL,
  `cmisUsername` varchar(255) DEFAULT NULL,
  `command` varchar(255) DEFAULT NULL,
  `commandAfter` varchar(255) DEFAULT NULL,
  `commandMaxLength` int(11) NOT NULL,
  `contentStoreId` int(11) NOT NULL,
  `creationDateTime` datetime DEFAULT NULL,
  `daysToKeepAfterLoad` int(11) NOT NULL,
  `deleteOption` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `destinationFolder` varchar(255) NOT NULL,
  `disablePackaging` bit(1) DEFAULT NULL,
  `emailMaxLength` int(11) NOT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `extensionMaxLength` int(11) NOT NULL,
  `inputSource` varchar(255) NOT NULL,
  `jobDescriptionMaxLength` int(11) NOT NULL,
  `jobNameMaxLength` int(11) NOT NULL,
  `lastModifyDateTime` datetime DEFAULT NULL,
  `listIgnorePath` bit(1) NOT NULL,
  `metadata` varchar(255) NOT NULL,
  `mode` varchar(255) NOT NULL,
  `moveAfterLoad` bit(1) DEFAULT NULL,
  `moveAfterLoadText` varchar(255) DEFAULT NULL,
  `moveBeforeProc` bit(1) DEFAULT NULL,
  `moveBeforeProcText` varchar(255) DEFAULT NULL,
  `moveNotLoad` bit(1) DEFAULT NULL,
  `moveNotLoadText` varchar(255) DEFAULT NULL,
  `moveUnprocessed` bit(1) DEFAULT NULL,
  `moveUnprocessedText` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `paramNameMaxLength` int(11) NOT NULL,
  `paramValueMaxLength` int(11) NOT NULL,
  `pathMaxLength` int(11) NOT NULL,
  `relaxedUnprocessedReporting` bit(1) DEFAULT NULL,
  `sendErrorReport` bit(1) NOT NULL,
  `sendErrorReportTo` varchar(255) DEFAULT NULL,
  `sendReport` bit(1) DEFAULT NULL,
  `sendReportTo` varchar(255) DEFAULT NULL,
  `skipContentUpload` bit(1) DEFAULT NULL,
  `transform` varchar(255) NOT NULL,
  `writeOption` varchar(255) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcq02rkgdk4i9hxtieswpvpgaq` (`creator_id`),
  KEY `FKeljv9v2lb99x0jt37qefyyxy3` (`destination_id`),
  CONSTRAINT `FKcq02rkgdk4i9hxtieswpvpgaq` FOREIGN KEY (`creator_id`) REFERENCES `userPswd` (`id`),
  CONSTRAINT `FKeljv9v2lb99x0jt37qefyyxy3` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (1,NULL,NULL,'','',5000,_binary '\0','','','','',255,-1,NULL,0,'SKIPANDIGNORE','Job example using the \'FilesystemMetadataAction\' parser. ','/Inflow/FilesystemMetadataAction',NULL,255,'*.pdf',255,'FILESYSTEM',255,50,'2019-04-17 10:56:22',_binary '\0','FilesystemMetadataAction','WRITE',_binary '\0',NULL,_binary '\0',NULL,_binary '\0',NULL,_binary '\0',NULL,'Example FilesystemMetadataAction PDF Job',50,255,255,_binary '\0',_binary '\0',NULL,_binary '\0',NULL,_binary '\0','notransformation','SKIPANDIGNORE',NULL,1);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processedDocumentLog`
--

DROP TABLE IF EXISTS `processedDocumentLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processedDocumentLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parameters` longtext,
  `processedDateTime` datetime DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `cycle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_name` (`name`),
  KEY `IDX_cycle_id_status` (`cycle_id`,`status`),
  KEY `IDX_cycle_id_processedDateTime` (`cycle_id`,`processedDateTime`),
  KEY `processedDocumentLog_IDX02` (`cycle_id`,`name`),
  CONSTRAINT `FKhdop443byank1dr92grc7pfma` FOREIGN KEY (`cycle_id`) REFERENCES `cycle` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processedDocumentLog`
--

LOCK TABLES `processedDocumentLog` WRITE;
/*!40000 ALTER TABLE `processedDocumentLog` DISABLE KEYS */;
INSERT INTO `processedDocumentLog` VALUES (1,'/data/FilesystemMetadataAction/input/test-3.pdf','[]','2019-04-17 10:59:15','','INPUTQUEUE',2),(2,'/data/FilesystemMetadataAction/input/test-1.pdf','[]','2019-04-17 10:59:15','','INPUTQUEUE',2),(3,'/data/FilesystemMetadataAction/input/test-2.pdf','[]','2019-04-17 10:59:15','','INPUTQUEUE',2),(4,'/data/FilesystemMetadataAction/input/test.pdf','[]','2019-04-17 10:59:15','','INPUTQUEUE',2),(5,'/data/FilesystemMetadataAction/input/test-3.pdf','[]','2019-04-17 10:59:17','workspace://SpacesStore/6a21e505-0b0f-4862-aeb7-10d8ba3b2c3f','OK',2),(6,'/data/FilesystemMetadataAction/input/test-1.pdf','[]','2019-04-17 10:59:17','workspace://SpacesStore/80f22cf7-b6de-4c66-8111-826156391a1d','OK',2),(7,'/data/FilesystemMetadataAction/input/test-2.pdf','[]','2019-04-17 10:59:17','workspace://SpacesStore/c00805ec-5528-4025-a004-52da77852424','OK',2),(8,'/data/FilesystemMetadataAction/input/test.pdf','[]','2019-04-17 10:59:17','workspace://SpacesStore/effe515c-4eeb-4a53-b15f-cb11fa424a28','OK',2);
/*!40000 ALTER TABLE `processedDocumentLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationDateTime` datetime DEFAULT NULL,
  `lastModifyDateTime` datetime DEFAULT NULL,
  `quartzScheduling` varchar(255) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcnb2d7c6nb92kpwoq3vkpt5p0` (`creator_id`),
  KEY `FK7srsrfnh5xwdowc31fgultubk` (`job_id`),
  CONSTRAINT `FK7srsrfnh5xwdowc31fgultubk` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  CONSTRAINT `FKcnb2d7c6nb92kpwoq3vkpt5p0` FOREIGN KEY (`creator_id`) REFERENCES `userPswd` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userPswd`
--

DROP TABLE IF EXISTS `userPswd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userPswd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `userName` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKia40v62t2027i48xq5m6eu83e` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userPswd`
--

LOCK TABLES `userPswd` WRITE;
/*!40000 ALTER TABLE `userPswd` DISABLE KEYS */;
INSERT INTO `userPswd` VALUES (1,'21232f297a57a5a743894a0e4a801fc3','admin');
/*!40000 ALTER TABLE `userPswd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userRole`
--

DROP TABLE IF EXISTS `userRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userRole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `userPswd_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrxq1d0jxp7ia5bcf52nmwp1t1` (`userPswd_id`),
  CONSTRAINT `FKrxq1d0jxp7ia5bcf52nmwp1t1` FOREIGN KEY (`userPswd_id`) REFERENCES `userPswd` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userRole`
--

LOCK TABLES `userRole` WRITE;
/*!40000 ALTER TABLE `userRole` DISABLE KEYS */;
INSERT INTO `userRole` VALUES (1,'ROLE_SCHEDULE_ADMIN','admin',1),(2,'ROLE_JOB_ADMIN','admin',1),(3,'ROLE_SYSTEM_ADMIN','admin',1),(4,'ROLE_CONSUMER','admin',1);
/*!40000 ALTER TABLE `userRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `document`
--

/*!50001 DROP VIEW IF EXISTS `document`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`inflow`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `document` AS select `dl`.`cycle_id` AS `cycle_id`,`dl`.`name` AS `name`,max(`dl`.`id`) AS `document_id` from (`cycle` `c` join `processedDocumentLog` `dl` on((`dl`.`cycle_id` = `c`.`id`))) where (`dl`.`name` <> 'Not a file') group by `dl`.`cycle_id`,`dl`.`name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-17 11:03:53
